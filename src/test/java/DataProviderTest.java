import io.qameta.allure.Epic;
import io.qameta.allure.Step;
import io.qameta.allure.Story;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import api.service.BookService;
import api.steps.BookSteps;

/*
     2. Подвязать allure, настроить аннотации (step, epic, story) и дополнительные если захотите
*/
@Epic("This is Book Test with Data Provider")
public class DataProviderTest {

    @Step("Clearing of book set")
    @BeforeTest
    void clearBooksOfUser() {
        new BookSteps().deleteBook();
    }

        @DataProvider(name = "books")
    public Object[][] getBooks() {
        return new Object[][] {
                {"Understanding ECMAScript 6", "Speaking JavaScript"},
                {"Learning JavaScript Design Patterns", "You Don't Know JS"},
                {"Programming JavaScript Applications", "Eloquent JavaScript, Second Edition"}
    };
}
    @Story("As a user I'd like to add to my book collection the first and the second book")
    @Step ("Add to my Book Collection {firstBook} and {secondBook}" )
    @Test(dataProvider = "books")
        void addBooksToUser(String firstBook,String secondBook) {
            //  1.добавить книгу пользователю
            String isbn = new BookService().getIsbnOfBook(firstBook);
            new BookSteps().addBookIntoUser(isbn);
            // проверить, что книга появилась
            new BookSteps().assertBookAppeared(firstBook);
            //  2.изменить книгу, заменив ее на другую
            isbn = new BookService().getIsbnOfBook(secondBook);
            new BookSteps().addBookIntoUser(isbn);
            // проверить, что книга появилась
            new BookSteps().assertBookAppeared(secondBook);
        }
    }

