import io.qameta.allure.*;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import api.service.BookService;
import api.steps.BookSteps;
import api.steps.UserSteps;

import static org.assertj.core.api.Assertions.assertThat;

@Epic("This is general epic")
public class BasicTest {


    @Test(description = "Get user with book or not", groups = {"Regression", "Api"})
    @Story("[1232-xml] As a user I want to get my own book user")
    @Link(name = "[1234-xml]", url = "https://google.com")
    @Issue("AUTH-123")
    void shouldBeUserReturned() {
        var user = new UserSteps().getUser();

        assertThat(user.getUsername()).as("Username is not correct").isEqualTo("user15");
    }
      @Step("Get all books")
    @Test(description = "Get all books", groups = {"Regression", "Api"})
    void getAllbooks(){
        System.out.println(new BookSteps().getAllBooks());
    }

    @DataProvider(name = "Books")
    public Object[][] getBooks() {
        return new Object[][] {
                {"You Don't Know JS"},
                {"Learning JavaScript Design Patterns"},
                {"Git Pocket Guide"}
        };
    }
}
